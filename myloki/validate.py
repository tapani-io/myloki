"""Module for validating user input."""


import re
import os

from settings import Paths


class Validate:
	"""Validate the user input."""
	
	def __init__(self):

		self.validation_data = {
			"errors": False,
			"message": ""
		}

	def chars(self, user_input):
		"""
		Validate characters. Allow only letters, numbers
		and some special characters.
		"""

		pattern = "^[a-z A-Z 0-9 \.\,\:\;\-\_\$\!\?\/\#\(\)]*$"

		if not re.match(pattern, user_input):
			self.validation_data.update(
				{
					"errors": True,
					"message": "Error: Invalid characters."
				}
			)
		
		return self.validation_data

	def length(self, user_input):
		"""Validate length. Less than 240, can't be empty."""

		if len(user_input) > 240:
			self.validation_data.update(
				{
					"errors": True,
					"message": "Error: Input is too long."
				}
			)
		if len(user_input) < 1:
			self.validation_data.update(
				{
					"errors": True,
					"message": "Error: Input can't be empty."
				}
			)
		
		return self.validation_data
	
	def log(self, log_file):
		"""Validate the log file. Must be found in data/log/."""
				
		log_path = os.path.join(Paths.LOGS_DIR, log_file)
		
		if os.path.isfile(log_path) == False:
				self.validation_data.update(
					{
						"errors": True,
						"message": "Error: Log file " + log_file + " doesn't exist."
					}
				)
		
		return self.validation_data
