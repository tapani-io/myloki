#!/usr/bin/env python3
#
# ===========================================================
# 
# myloki
#
# keep a simple log of any kind via the command-line.
#
# Author: Tapani Io
# Email: tapani.io@protonmail.com
# GitLab: https://gitlab.com/tapani-io
#
# License: MIT
#
# ===========================================================


import os
import time
import argparse
import re
from datetime import datetime, date, timedelta
from shutil import copy

from settings import Paths
from validate import Validate


def get_args():
	"""Get command-line arguments to determine action."""

	parser = argparse.ArgumentParser(description="Keep a log of any kind via the command-line")
	
	# Add a group for arguments that can't be used together.
	parser_excl = parser.add_mutually_exclusive_group()
	
	parser_excl.add_argument("--add", dest="add_entry", action="store", help="Add a log entry.")
	parser_excl.add_argument("--find", dest="find_entry", action="store", help="Find a specific log entry.")	
	parser_excl.add_argument("--find-last", dest="find_last", action="store", help="Find the last entry that contains a specific keyword.")	
	parser_excl.add_argument("--view", dest="view_all", action="store_true", help="View all log entries.")
	parser_excl.add_argument("--tail", dest="view_tail", action="store_true", help="View the last log entry")
	parser_excl.add_argument("--export", dest="export_log", action="store_true", help="Export log to current directory.")
	
	parser.add_argument("--log", dest="choose_log", action="store", default="default", help="Choose log you wish to operate on.")

	args = parser.parse_args()
	
	return args


def validate_input(user_input):
	"""Validate the user input with Validate module."""
	
	check_chars = Validate().chars(user_input)
	check_length = Validate().length(user_input)
	
	if check_chars["errors"] == True:
		return check_chars["message"]
	elif check_length["errors"] == True:
		return check_length["message"]
	else:
		return "Valid"


def validate_log(log_file):
	"""Validate the log file with Validate module."""

	check_logfile = Validate().log(log_file)

	if check_logfile["errors"] == True:
		return check_logfile["message"]
	else:
		return "Valid"
	
	
def get_timestamp():
	"""Get current timestamp for log entry."""

	timestamp = datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d_%H-%M-%S")
	return timestamp


def choose_log(user_input):
	"""Choose log file to work with."""

	log_file = os.path.join(Paths.LOGS_DIR, user_input)

	return log_file


def create_log(timestamp, log_file):
	"""Create a new log file if doesn't exist."""
	
	output_text = timestamp + ", Log file created \n"
	
	with open(log_file, "w") as output:
		output.write(output_text)
	
	print("\nLog file created: " + log_file)


def export_log(timestamp, log_file):
	"""Export the log to current directory."""
	
	output_file = os.path.join(os.getcwd(), f"{timestamp}-log-export.txt")
	
	try:
		copy(log_file, output_file)
		print("Log exported to " + output_file)
	except FileNotFoundError:
		print("Error: " + log_file + " doesn't exist.")


def read_log(log_file):
	"""Open and read the log file."""
		
	try:
		with open(log_file, "r") as read_file:
			content = read_file.readlines()
			return content
	except FileNotFoundError:
		print("Error: " + log_file + " doesn't exist.")


def view_all(log_file):
	"""View all log entries."""
	
	for row in read_log(log_file):
		timestamp = row.split(",")[0]
		entry =row.split(",")[1]
		print(timestamp + ": " + entry)


def view_tail(log_file):
	"""View the last log entry."""
	
	print(read_log(log_file)[-1])


def find_entry(user_input, log_file):
	"""Find a specific log entry."""

	for row in read_log(log_file):
		match = re.search(user_input, row)
		
		if match != None:
			print(row)
		else:
			pass


def add_entry(timestamp, user_input, log_file):
	"""Add a log entry to log file."""

	output_text = timestamp + "," + user_input + "\n"
	
	with open(log_file, "a") as output:
		output.write(output_text)

	print("\nEntry saved to log:\n" + output_text)


def main():
	"""Run functions."""

	args = get_args()
	
	log_file = choose_log(args.choose_log) + ".csv"
	log_validation = validate_log(log_file)
	
	if log_validation == "Valid":

		if args.add_entry:
			
			user_input = args.add_entry		
			input_validation = validate_input(user_input)
		
			if input_validation == "Valid":
				add_entry(get_timestamp(), user_input, log_file)
			else:
				print(input_validation)

		if args.export_log:
			export_log(get_timestamp(), log_file)

		if args.view_all:
			view_all(log_file)

		if args.view_tail:
			view_tail(log_file)

		if args.find_entry:

			user_input = args.find_entry
			input_validation = validate_input(user_input)
			
			if input_validation == "Valid":
				find_entry(user_input, log_file)
			else:
				print(input_validation)
	
	else:
		print("\n" + log_validation)
		print("\nDo you want to create the log? \n")
		
		prompt_log = input("yes/no: ")
		
		if prompt_log == "yes":
			create_log(get_timestamp(), log_file)
		else:
			pass


if __name__ == "__main__":
	main()
