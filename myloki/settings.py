"""Module for program settings."""


import os
from pathlib import Path


class Paths:
	
	APP_PATH = os.path.realpath(__file__)
	APP_DIR = Path(APP_PATH).parents[1]
	DATA_DIR = os.path.join(APP_DIR, "data")
	LOGS_DIR = os.path.join(DATA_DIR, "log")
	BACKUP_DIR = os.path.join(DATA_DIR, "backup")
