# Contributing

If you like myloki, you can totally contact me by just saying "Thanks!". Programmers can't hear that enough! 

When you notice a bug, or a typo, or even have a recommendation how to make the code better, please open an issue to discuss it. If you want, you can even work on the code yourself and open a pull request.

