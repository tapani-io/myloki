import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="myloki-YOUR-USERNAME-HERE",
    version="0.1",
    author="Tapani Io",
    author_email="tapani.io@protonmail.com",
    description="Keep a simple log of any kind via the command-line.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tapani-io/myloki",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
