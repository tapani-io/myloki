# myloki

Keep a simple log of any kind via the command-line.

## Usage

**1. Clone the repository:**

	git clone https://gitlab.com/tapani-io/myloki.git

**2. Run program:**

	python3 myloki/myloki.py --command

## Commands

Add a log entry:

	--add "Hello world"

Find a log entry with a keyword:

	--find "Search term"

Find the last log entry that contains a specific keyword:

	--find-last "Search term"

View all log entries:

    --view

View last log entry:

	--tail

Export log file:

    --export

Use another log file (instead of default):

    --log "log name"
    
View all commands:

	--help
    
# Contributing

If you like myloki, you can totally contact me by just saying "Thanks!". Programmers can't hear that enough! 

When you notice a bug, or a typo, or even have a recommendation how to make the code better, please open an issue to discuss it. If you want, you can even work on the code yourself and open a pull request.

